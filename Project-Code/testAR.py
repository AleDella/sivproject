import os
import numpy as np
import argparse
import torch.cuda
from torch.autograd import Variable
import GrabCut
import cv2
import webcam

if __name__ == "__main__":

    #####yolov4 model loading########
    net = cv2.dnn.readNet("yolov2-tiny.weights", "yolov3.cfg")

    classes = []
    with open("coco.names", "r") as f:
        classes = [line.strip() for line in f.readlines()]

    layer_names = net.getLayerNames()

    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    ###########################

    torch.cuda.empty_cache()
    ##############PARSER OPTIONS##################
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_dir', default='video')
    parser.add_argument('--model_path', default='./pretrained_model')

    # parser.add_argument('--output_dir', default = 'test_output')
    parser.add_argument('--gpu', type=int, default=-1)

    opt = parser.parse_args()
    ######################################
    valid_ext = ['.jpg', '.png']

    
    print(torch.version.cuda)
    if torch.cuda.is_available():
        print("pr")
        dev = "cuda:0"
    else:
        print("no pr")
        dev = "cpu"

    device = torch.device(dev)
    print(device)
    


    cap = cv2.VideoCapture(0)  # webcam image in which we will find the target

    # Detector: thing that detect features in an image and translate them into descriptors
    # In this case we used a free detector called ORB
    orb = cv2.ORB_create(nfeatures=1000)  # initialize the detector

    while (cap.isOpened()):
        # Webcam caption
        success, imgWebcam = cap.read()
        dsize = (int(imgWebcam.shape[1]*0.5), int(imgWebcam.shape[0]*0.5))
        imgWebcam = cv2.resize(imgWebcam, dsize, interpolation=cv2.INTER_NEAREST)
        
        imgAug = imgWebcam.copy()
        input_image_cv = imgAug.copy()

        #kp2 contains the keypoints which are translated into the descriptor des2
        kp2, des2 = orb.detectAndCompute(imgWebcam, None)

        ######Contour detection and image cropping################


        height, width, channels = input_image_cv.shape
        blob = cv2.dnn.blobFromImage(input_image_cv, 1 / 255.0, (416, 416), swapRB=True, crop=False)
        net.setInput(blob)
        outs = net.forward(output_layers)
        class_ids = []
        confidences = []
        boxes = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    # Object detected
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)

                    # Rectangle coordinates
                    x = int(center_x - w / 2)
                    y = int(center_y - h / 2)

                    boxes.append([x, y, w, h])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)

        indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
        color = [235.58333418, 32.37273083, 63.18535565]  # This is blue

        im1 = input_image_cv.copy()
        if len(boxes) != 0:
            crop_im = im1[(boxes[0][1]):(boxes[0][1] + boxes[0][3]), (boxes[0][0]):(boxes[0][0] + boxes[0][2])]
        else:
            crop_im = im1
        imgTarget, detected = GrabCut.GrabCut(crop_im)# GrabCut application
        # Now in crop_im there is the detected object in an image
        ###########################################################################à

        if detected==True:
            ############## Padding Procedure###############à
            if len(boxes) != 0:
                padTop = height - boxes[0][1] - boxes[0][3]
                if padTop < 0:
                    padTop = 0
                padBottom = boxes[0][1]
                if padBottom < 0:
                    padBottom = 0
                padLeft = width - boxes[0][0] - boxes[0][2]
                if padLeft < 0:
                    padLeft = 0
                padRight = boxes[0][0]
                if padRight < 0:
                    padRight = 0
                imgTarget = cv2.copyMakeBorder(imgTarget, padBottom, padTop,
                                      padRight, padLeft, cv2.BORDER_CONSTANT, 0)
            ###########################################
            kp1, des1 = orb.detectAndCompute(imgTarget, None)
            #image target not found
            if (len(imgTarget.shape)==2):
                hT, wT = imgTarget.shape
            #image target has been found
            else:
                hT, wT, _ = imgTarget.shape
            # Cartoonization
            output_image = webcam.webcam (imgTarget)
            
            imgCart = np.array(output_image)
            
            # matching between the two images using knn brute force
            bf = cv2.BFMatcher()
            matches = bf.knnMatch(des1, des2, k=2)
            good = []
            for m, n in matches:
                if m.distance < 0.75 * n.distance:
                    good.append(m)
            
            # Homography: finding a relationship between same points in the train image and in the query image
            # the relationship is represented as a matrix
            # check if the length of good matches is enough to begin with Homography
            if len(good) > 15:
                src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
                dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
                matrix, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
                pts = np.float32([[0, 0], [0, hT], [wT, hT], [wT, 0]]).reshape(-1, 1, 2)  # corner points
                dst = cv2.perspectiveTransform(pts, matrix)  # find the destination points with corner points and rel matrix
                
                # next step is to add a black square above the original image in order to have a more realistic augmentation
                maskNew = np.zeros((imgAug.shape[0], imgAug.shape[1]), np.uint8)
                # create blank image of Augmentation size
                cv2.fillPoly(maskNew, [np.int32(dst)], (255, 255, 255, 0.1))
                # fill the detected area with white pixels to get mask with a white square on center ehre the target is located
                imgAug = cv2.bitwise_and(imgAug, imgAug, mask=maskNew)
                
                imgCart = cv2.normalize(imgCart, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8UC3)

                imgFin = GrabCut.util(imgCart, imgAug, imgTarget)
        else:
            imgFin = input_image_cv
        cv2.imshow("Final Result", imgFin)
        if cv2.waitKey(1) == ord("q"):
            break
cap.release()
cv2.destroyAllWindows()
exit(0)
