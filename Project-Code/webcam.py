import transformer
import torch
import utils
#import tensorflow

STYLE_TRANSFORM_PATH = "transforms/mosaic.pth"


def webcam(img):
    """
    Captures and saves an image, perform style transfer, and again saves the styled image.
    Reads the styled image and show in window.
    """
    # Device
    device = ("cuda" if torch.cuda.is_available() else "cpu")

    net = transformer.TransformerNetwork()
    net.load_state_dict(torch.load(STYLE_TRANSFORM_PATH))
    net = net.to(device)

    # Free-up unneeded cuda memory
    torch.cuda.empty_cache()

    # Generate image
    content_tensor = utils.itot(img).to(device)
    generated_tensor = net(content_tensor)
    generated_image = utils.ttoi(generated_tensor.detach())
    generated_image = utils.transfer_color(img, generated_image)
    generated_image = generated_image / 255
    return generated_image

